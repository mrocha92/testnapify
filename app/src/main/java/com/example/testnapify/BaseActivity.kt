package com.example.testnapify

import android.content.Intent
import android.nfc.Tag
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.Toast
import com.example.testnapify.fragments.ProductoFragment
import com.example.testnapify.fragments.TiendaFragment
import kotlinx.android.synthetic.main.activity_base.*

class BaseActivity : AppCompatActivity() {

    var ll_content : LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        ll_content = findViewById(R.id.ll_content) as LinearLayout
        replaceFragment(TiendaFragment())
        initActionbar()
}

    private fun initActionbar() {

        setSupportActionBar(toolbar)
        getSupportActionBar()!!.hide()
        val actionBar = supportActionBar
        actionBar!!.title = "TestNapify"
        actionBar.subtitle = "Listas de tiendas"
        actionBar.elevation = 4.0F
        actionBar.setDisplayShowHomeEnabled(true)
        actionBar.setLogo(R.mipmap.ic_launcher)
        actionBar.setDisplayUseLogoEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_establishment -> {

                getSupportActionBar()!!.hide()
                replaceFragment(TiendaFragment())

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_products -> {
                getSupportActionBar()!!.show()
                replaceFragment(ProductoFragment())

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_ubication -> {
                val intent = Intent(this, UbicationActivity::class.java)

/*
val latitude: Double = latitude.VariableScript // or just your string
val longitude: Double = longitude.VariableScript // or just your string

 val intent = Intent(this, UbicationActivity::class.java)
intent.putExtra("latitude", latitude)
intent.putExtra("longitude", longitude)
startActivity(intent)
*/


                startActivity(intent)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.ll_content, fragment)
                .addToBackStack(null)
                .commit()
    }

}
