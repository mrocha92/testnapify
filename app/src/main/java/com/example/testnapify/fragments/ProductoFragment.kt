package com.example.testnapify.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.*
import com.example.testnapify.R
import com.example.testnapify.adapters.ProductoAdapter
import com.example.testnapify.retrofit.RequestInterface
import com.example.testnapify.resources.Products
import com.example.testnapify.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList

class ProductoFragment : Fragment() ,AdapterView.OnItemSelectedListener {


    lateinit var rootView: View
    internal lateinit var frame: Toolbar
    private lateinit var rv_Producto: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager

    var lstProducto: MutableList<Products> = ArrayList()

    lateinit var requestInterface:RequestInterface
    lateinit var productoAdapter: ProductoAdapter
    var compositeDisposable = CompositeDisposable()


    internal lateinit var sp_sort: Spinner
    internal  var listSortBy = arrayOf("","Nombre A-Z", "Precio","Nombre Z-A")
    fun selectorPrice(p: Products): Int = p.price
    fun selectorName(p: Products): String = p.name

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_producto, container, false)
        frame = activity!!.findViewById(R.id.toolbar) as Toolbar

        val retrofit = RetrofitClient.getInstance
        requestInterface = retrofit.create(RequestInterface::class.java!!)

       initElementsRV()
        initSpinner()

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        activity!!.menuInflater.inflate(R.menu.toolbarmenu,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id=item!!.itemId
        return if (id == R.id.action_search){
            true
        }else{
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true);
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val menuItem = menu.findItem(R.id.action_search)
        val search = menuItem.actionView as SearchView
        searching(search)
        super.onPrepareOptionsMenu(menu)
    }

    //********************************filtra los cards segun las letras que se introducen************************
    private fun searching(search: SearchView) {
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                productoAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                productoAdapter.filter.filter(newText)
                return true
            }
        })
    }

    private fun initSpinner() {
        sp_sort =rootView.findViewById(R.id.sp_sortBy)
        val dataAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item,listSortBy)
        sp_sort.setAdapter(dataAdapter)
        sp_sort.setOnItemSelectedListener(this)
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        if (position.equals(0)){

        }else if (position.equals(1)){//******************Nombre
            val sortedProducts = lstProducto.sortedBy({selectorName(it)})
            sortedProducts.forEach{println(it)}
            productoAdapter = ProductoAdapter(sortedProducts.toList(),getContext()!!)
           loadRecyclerView()

        }else if (position.equals(2)) {//******************Precio
            val sortedProducts = lstProducto.sortedBy({selectorPrice(it)})
            sortedProducts.forEach{println(it)}
            productoAdapter = ProductoAdapter(sortedProducts.toList(),getContext()!!)
            loadRecyclerView()

        }else if (position.equals(3)) {//******************Descendente
            val sortedProducts = lstProducto.sortedByDescending  {selectorName(it)}
            sortedProducts.forEach{println(it)}
            productoAdapter = ProductoAdapter(sortedProducts.toList(),getContext()!!)
            loadRecyclerView()
        }

    }

    private fun initElementsRV() {
        lstProducto = ArrayList()
        viewManager = LinearLayoutManager(getContext())
        productoAdapter = ProductoAdapter(lstProducto,getContext()!!)
        loadRecyclerView()
        fetchData()
    }
    private fun fetchData(){
        compositeDisposable.add(requestInterface.getproducts
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { getproducts -> displayData(getproducts) })
    }

    private fun displayData(getproducts: List<Products>?) {
        lstProducto.clear()
        lstProducto. addAll(getproducts!!)
        productoAdapter .notifyDataSetChanged()

    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    private fun loadRecyclerView(){
        productoAdapter.notifyDataSetChanged()//notifica el cambio
        rv_Producto =rootView.findViewById<RecyclerView>(R.id.rv_producto).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = productoAdapter

        }
        rv_Producto.layoutManager = LinearLayoutManager(context)
    }
}
