package com.example.testnapify.fragments


import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testnapify.R
import com.example.testnapify.adapters.TiendaAdapter
import com.example.testnapify.retrofit.RequestInterface
import com.example.testnapify.resources.Establishment
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.ArrayList


class TiendaFragment : Fragment() {

       internal lateinit var lstTienda: List<Establishment>
        lateinit var rootView: View
       private lateinit var  rv_tienda: RecyclerView
        private lateinit var tiendaAdapter: RecyclerView.Adapter<*>
        private lateinit var viewManager: RecyclerView.LayoutManager

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
            rootView = inflater.inflate(R.layout.fragment_tienda, container, false)
            lstTienda = ArrayList()
            viewManager = LinearLayoutManager(getContext())
            tiendaAdapter = TiendaAdapter(lstTienda,getContext()!!)
            tiendaAdapter.notifyDataSetChanged()//notifica el cambio


            rv_tienda =rootView.findViewById<RecyclerView>(R.id.rv_tienda).apply {
                setHasFixedSize(true)
                layoutManager = viewManager
                adapter = tiendaAdapter
            }
            rv_tienda.layoutManager = LinearLayoutManager(context)
            loadJSON()

            return rootView
        }
    fun loadJSON() {
        try {

            val handler = HandlerThread("TiendaHandler")
            handler.start()
            val retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("http://private-2a065d-testnapify.apiary-mock.com")
                    .client(OkHttpClient.Builder().build())
                    .build()
            val postApi = retrofit.create(RequestInterface::class.java)
            postApi.getEstablishments()
            var response = postApi.getEstablishments()

            response.observeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.from(handler.looper))
                    .subscribe({
                        Handler(Looper.getMainLooper()).post({
                            rv_tienda.adapter = TiendaAdapter(it, this!!.context!!)
                        })

                    },{
                        Log.d("Error","Ah que caray")
                    })
        }catch (e: Exception){

        }
    }

}