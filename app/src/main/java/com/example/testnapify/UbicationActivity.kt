package com.example.testnapify

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.BaseTransientBottomBar.LENGTH_INDEFINITE
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.view.View
import com.example.testnapify.BuildConfig.APPLICATION_ID
import com.example.testnapify.resources.Location
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Marker

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.testnapify.VariableScript
import android.view.Gravity
import android.widget.*


class UbicationActivity : AppCompatActivity(), OnMapReadyCallback , GoogleMap.OnMarkerClickListener
     {
         override fun onMarkerClick(p0: Marker?): Boolean {
             TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
         }

    private lateinit var mMap: GoogleMap

    private  var TAG ="UserLocation"
    internal var us = VariableScript()

        private val LOCATION_PERMISSION_REQUEST_CODE =34


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ubication)

/**
var bundle :Bundle ?=intent.extras
var latitude = bundle!!.getDouble("latitude") // 1
var longitude = bundle!!.getDouble("longitude") // 1
var latitude: Double = intent.getDoubleExtra("latitude") // 2
var longitude: Double = intent.getDoubleExtra("longitude") // 2
Toast.makeText(this, latitude.toString Toast.LENGTH_SHORT).show()
*/
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }



         override fun onStart() {
        super.onStart()
        if (!checkPermissions()) {
            requestPermissions()
        } else {
           locationStart()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val sydney = LatLng(Location().ubication.longitude,Location().ubication.latitude)
        // val sydney = LatLng(longitude,latitude)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker "))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        Log.d("UserLocation", Location().ubication.longitude.toString());
        Log.d("UserLocation",Location().ubication.latitude.toString());

        mMap.setOnMarkerClickListener(this)
        mMap.uiSettings.isZoomControlsEnabled = true

    }
         override fun onRequestPermissionsResult(
                 requestCode: Int,
                 permissions: Array<String>,
                 grantResults: IntArray
         ) {
             Log.i(TAG, "onRequestPermissionResult")
             if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
                 when {
                     grantResults.isEmpty() -> Log.i(TAG, "User interaction was cancelled.")

                 // Permission granted.
                     (grantResults[0] == PackageManager.PERMISSION_GRANTED) -> locationStart()//getLastLocation()

                     else -> {
                         showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                                 View.OnClickListener {

                                     val intent = Intent().apply {
                                         action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                         data = Uri.fromParts("package", APPLICATION_ID, null)
                                         flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                     }
                                     startActivity(intent)
                                 })
                     }
                 }
             }
         }private fun showSnackbar(
             snackStrId: Int,
             actionStrId: Int = 0,
             listener: View.OnClickListener? = null
     ) {
         val snackbar = Snackbar.make(findViewById(android.R.id.content), getString(snackStrId),
                 LENGTH_INDEFINITE)
         if (actionStrId != 0 && listener != null) {
             snackbar.setAction(getString(actionStrId), listener)
         }
         snackbar.show()
     }

         fun locationStart() {
             val mlocManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
             val Local = Location()
             //Local.setMainActivity(this)
             Local.UbicacionActivity
             val gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
             if (!gpsEnabled) {
                 val settingsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                 startActivity(settingsIntent)
             }
             if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                 ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1000)
                 return
             }
             mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, Local as LocationListener)
             mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, Local as LocationListener)

         }


    fun checkPermissions() =
            ActivityCompat.checkSelfPermission(this,  Manifest.permission.ACCESS_FINE_LOCATION) ==PackageManager.PERMISSION_GRANTED
    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
    }

    private fun requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            // Provide an additional rationale to the user. This would happen if the user denied the
            // request previously, but didn't check the "Don't ask again" checkbox.
            Log.i(TAG, "Displaying permission rationale to provide additional context.")



            /////////////////////////*************Muestra mensaje de que requiere permisos****/************************************************

        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            Log.i(TAG, "Requesting permission")
            startLocationPermissionRequest()
        }
    }



}
