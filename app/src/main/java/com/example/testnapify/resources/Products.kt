package com.example.testnapify.resources

data class Products(
    val description: String,
    val id: Int,
    val name: String,
    val price: Int
)