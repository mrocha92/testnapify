package com.example.testnapify.resources

import com.google.gson.annotations.SerializedName

data class Establishment(

    @SerializedName("id") val id: Int,
    @SerializedName("address") val address: String,
    @SerializedName("image") val image: String,
    @SerializedName("name") val name: String
)