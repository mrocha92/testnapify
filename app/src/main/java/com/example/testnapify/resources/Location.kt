package com.example.testnapify.resources

import android.location.Location
import android.location.LocationListener
import android.location.LocationProvider
import android.os.Bundle
import android.util.Log
import com.example.testnapify.UbicationActivity
import com.example.testnapify.VariableScript
import com.example.testnapify.latitudeClass
import com.example.testnapify.longitudeClass


class Location : LocationListener {
     var UbicacionActivity: UbicationActivity?=null
     var ubication = VariableScript()


    override fun onLocationChanged(loc: Location) {
        loc.latitude
        loc.longitude

       ubication.longitude = loc.longitude
        ubication.latitude = loc.latitude

        // val miObjeto = longitudeClass (loc.longitude)
      //   val miObjeto2 = latitudeClass (loc.latitude)

    //    Log.d("UserLocation",  ubication.longitude.toString());
      //  Log.d("UserLocation", ubication.latitude.toString());

    }


    override fun onProviderDisabled(provider: String) {
        Log.d("UserLocation", "GPS Desactivado")
    }

    override fun onProviderEnabled(provider: String) {
        Log.d("UserLocation", "GPS Activado")
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        when (status) {
            LocationProvider.AVAILABLE -> Log.d("debug", "LocationProvider.AVAILABLE")
            LocationProvider.OUT_OF_SERVICE -> Log.d("debug", "LocationProvider.OUT_OF_SERVICE")
            LocationProvider.TEMPORARILY_UNAVAILABLE -> Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE")
        }
    }


}