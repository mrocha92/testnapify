package com.example.testnapify.adapters

import android.content.ClipData
import android.content.ClipData.Item
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.testnapify.R

import android.content.Context
import android.widget.Filter
import android.widget.Filterable
import com.example.testnapify.resources.Products

class ProductoAdapter (private var productosList: List<Products>, val context: Context)
    : RecyclerView.Adapter<ProductoAdapter.ViewHolder>() ,Filterable{

    internal  var filterListResult :List<Products>
    init {
        this.filterListResult = productosList
    }
    override fun getFilter(): Filter {
     return object :Filter(){
         override fun performFiltering(charString: CharSequence?): FilterResults {
             val charSearch : String = charString.toString()
             if (charSearch.isEmpty())
                 filterListResult = productosList
             else{
                 val resultList = ArrayList<Products>()
                 for (row  in productosList){

                     if (row.name!!.toLowerCase().contains(charSearch.toLowerCase()))
                         resultList.add(row)

                 }
                 filterListResult = resultList
             }
             val filterResults =Filter.FilterResults()
             filterResults.values = filterListResult
             return  filterResults
         }

         override fun publishResults(charSequence: CharSequence, filterResults: FilterResults?) {
            filterListResult = filterResults!!.values as List<Products>
             notifyDataSetChanged()
         }
     }
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ProductoAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.producto_item, viewGroup, false)
        return ViewHolder(view)
    }
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {


        viewHolder.tv_nameP.text =filterListResult.get(position).name
        viewHolder.tv_descriptionP.text =filterListResult.get(position).description
        viewHolder.tv_priceP.text =filterListResult.get(position).price.toString()



    }
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_nameP: TextView
        val tv_descriptionP: TextView
        val tv_priceP : TextView
        init {

            tv_nameP = view.findViewById(R.id.tv_nameP) as TextView
            tv_descriptionP = view.findViewById(R.id.tv_descriptionP) as TextView
            tv_priceP = view.findViewById(R.id.tv_priceP)as TextView
        }
    }
    override fun getItemCount() = filterListResult.size
}