package com.example.testnapify.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.testnapify.R
import com.example.testnapify.resources.Establishment


class TiendaAdapter (private val tiendasList: List<Establishment>, val context: Context )
    : RecyclerView.Adapter<TiendaAdapter.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): TiendaAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.tienda_item, viewGroup, false)
        return ViewHolder(view)
    }
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.tv_nameT.text =tiendasList.get(position).name
        viewHolder.tv_addressT.text =tiendasList.get(position).address

        Glide.with( context )
                .load(tiendasList.get(position).image)

                .into(viewHolder.iv_imageT)

    }
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
         val tv_nameT: TextView
         val iv_imageT: ImageView
         val tv_addressT: TextView

        init {

            tv_nameT = view.findViewById(R.id.tv_nameT) as TextView
            iv_imageT = view.findViewById(R.id.iv_imageT) as ImageView
            tv_addressT = view.findViewById(R.id.tv_addressT) as TextView

        }
    }


    override fun getItemCount() = tiendasList.size



}
