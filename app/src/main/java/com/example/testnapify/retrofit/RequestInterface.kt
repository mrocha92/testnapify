package com.example.testnapify.retrofit

import com.example.testnapify.resources.Establishment
import com.example.testnapify.resources.Products
import io.reactivex.Observable

import retrofit2.http.GET


interface RequestInterface {
    @GET("/establishments")
    fun getEstablishments():Observable <List<Establishment>>

    @get:GET("/products")
    val getproducts:Observable <List<Products>>

}