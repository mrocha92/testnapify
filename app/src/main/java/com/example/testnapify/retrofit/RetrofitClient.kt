package com.example.testnapify.retrofit

import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import com.example.testnapify.R.id.rv_producto
import com.example.testnapify.adapters.ProductoAdapter
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_producto.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private var instance:Retrofit?= null
    val getInstance:Retrofit
    get() {
        if (instance == null)
            instance = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("http://private-2a065d-testnapify.apiary-mock.com")
                    .client(OkHttpClient.Builder().build())
                    .build()
            return instance!!
        }
}
